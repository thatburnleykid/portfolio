import React from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Start from './sections/Start';
import Game from './sections/Games';
import Web from './sections/Web';
import Contact from './sections/Contact';
import Navigation from './components/navigation';

import './App.css';

export default () => (
  <div className="App">
    <Router>
    <Navigation />
      <Switch>
        <Route path='/' exact render={(props) => <Start {...props} />}/>
        <Route path='/games' exact render={(props) => <Game {...props} />}/>
        <Route path='/web' exact render={(props) => <Web {...props} />}/>
        <Route path='/contact' exact render={(props) => <Contact {...props} />}/>
      </Switch>
    </Router>
  </div>
);