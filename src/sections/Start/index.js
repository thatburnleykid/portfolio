import React from 'react';
import Typist from 'react-typist';
import Scroll from '../../components/scroll';

import './start.scss';
import 'react-typist/dist/Typist.css';

export default () => {
    return(
        <div id='start'>
            <div className='centerMeDaddy'>
                <Typist cursor={{ show: false, hideWhenDone: true }}>
                    <Typist.Delay ms={1000} />
                    <div className='title'>Dev.</div>
                    <Typist.Delay ms={500} />
                    <div className='subtitle'>web + game</div>
                </Typist>
            </div>
            <Scroll />
        </div>
    );
}