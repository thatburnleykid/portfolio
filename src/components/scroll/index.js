import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDoubleDown } from '@fortawesome/fontawesome-free-solid'
import './scroll.scss';


export default () => (
    <div className='scroll'>
        <div>Scroll</div>
        <FontAwesomeIcon icon={faAngleDoubleDown}/>
    </div>
);