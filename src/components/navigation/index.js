import React from 'react';
import { Link } from 'react-router-dom';

import './navigation.scss';

export default () => (
    <nav>
        <div className='sitename'>
            <Link classNAme="link" to='/'>michael burnley</Link>
        </div>
        <div className='links'>
            <Link className="link" to='/contact'>contact</Link>
            <Link className="link" to='/games'>games</Link>
            <Link className="link" to='/web'>web</Link>
        </div>
    </nav>
);